    function WorkWithCommentForm(idObject, idInstance, count, showCaptcha, commentAreaClass) {
      this.idObject = idObject;
      this.idInstance = idInstance;
      this.count = count;
      this.showCaptcha = showCaptcha;
      
      if (typeof(commentAreaClass) == "undefined") commentAreaClass = "cCommentArea";
      this.commentAreaClass = commentAreaClass;
      this.commentArea = $("." + commentAreaClass);

    	this.formClass = "cSendForm";
    	this.commentClass = "cCommentList";
    	this.commentCount = "cCommentTag";
    }
    
    WorkWithCommentForm.prototype = {
      //Отсылка формы
      sendCommentForm: function() {
        var form = $("form." + this.formClass);
        //Отключить активность кнопки
        form.find(":button").attr("disabled", "disabled");
        //Показать вертелку
        form.find(".cCommentProcess").show();
      
        var values = new Object();
        
        values["commentAreaClass"] = this.commentAreaClass;
        values["formClass"] = this.formClass;
        values["commentClass"] = this.commentClass;
        values["commentCount"] = this.commentCount;
        
        form.find("input,textarea,select").each(function() {
          //values[$(this).attr("name")] = $.trim($(this).val());
          var value = $.trim($(this).val());
          if (this.tagName.toLowerCase() == "textarea") {
            //if tinyMCE
            var ifr = $('#' + $(this).attr('id') + '_ifr');
            if (ifr.length > 0) {
              var value = document.getElementById(ifr.attr('id')).contentWindow.document.body.innerHTML;
              /*
               * <p>
               * авып
               * <img src="http://doska.bt/engine/admin/TinyMCE/plugins/emotions/img/smiley-cool.gif" _mce_src="/engine/admin/TinyMCE/plugins/emotions/img/smiley-cool.gif" alt="Круто!" title="Круто!" border="0">
               * <br _mce_bogus="1">
               * </p>
               */
              value = value.replace(/\s*_mce[^\s>]+/g, '');
              value = value.replace(/\<p\>\<br\>\<\/p\>/g, '');
            }
          }
          values[$(this).attr("name")] = value;
        });
        
        //Проверка данных формы
        da_formCheck(values, this.count);
      },
      
      //Перелистывание комментариев
      listPages: function(page) {
        da_listingComment(this.idObject, this.idInstance, this.count, page);
      }
    }
    
    
  function HierarchicalJSFunctions(text_svernut, text_razvernut, text_svernut_all, text_razvernut_all, text_svernut_vetku, text_razvernut_vetku, commentAreaClass) {
      this.text_svernut = text_svernut;
      this.text_razvernut = text_razvernut;
      this.text_svernut_all = text_svernut_all;
      this.text_razvernut_all = text_razvernut_all;
      this.text_svernut_vetku = text_svernut_vetku;
      this.text_razvernut_vetku = text_razvernut_vetku;

      if (typeof(commentAreaClass) == "undefined") commentAreaClass = 'cCommentArea';
      this.commentAreaClass = commentAreaClass;
      this.commentArea = $("." + commentAreaClass);
      var form = this.commentArea.find("form");
      (form.length) ? this.form = form : alert("Не найдена форма внутри конейнера с комменариями. \nКласс контейнера " + commentAreaClass);
  }
  
  HierarchicalJSFunctions.prototype = {
    //Развернуть тело определённого комментария
    showCommentBody: function (id) {
          var c = $(".commenthtml_" + id + ' .commentbody');
          if (c.css("display") == "none") {
            $(".commenthtml_" + id + ' .minimize').html(this.text_svernut);
          } else {
            $(".commenthtml_" + id + ' .minimize').html(this.text_razvernut);
          }
          c.toggle();
    },
  
    //Развернуть/Свернуть всю ветку
    showCommentBranch: function(id) {
      if ($(".commenthtml_" + id + " .minimize").html() == this.text_razvernut_vetku) {
        var display = "block";
        var t       = this.text_svernut;
        var t2      = this.text_svernut_vetku;
      } else {
        var display = "none";
        var t       = this.text_razvernut;
        var t2      = this.text_razvernut_vetku;
      }
  
      $(".commenthtml_" + id).parent().find('ul .commentbody').css("display", display).end();
      
      $(".commenthtml_" + id).next('ul').find('.item').each(function () {
        var elem = $(this).find('.minimize');
        if ($(this).next('ul').length) {
          //Имеем дело с веткой
          elem.html(t2);
        } else {
          //Одиночный узел
          elem.html(t);
        }
      });
      
      //Сама ссылка, на кот. нажали
      $(".commenthtml_" + id + ' .minimize').html(t2);
    },
    
    //Развернуть все комментарии
    showAllCommentsBody: function() {
      var turnOnOff = this.commentArea.find(".commentsTurnOnOff");
      if (turnOnOff.length > 0) {
	      if (turnOnOff.html() != this.text_svernut_all) {
	          var display = "block";
	          //Меняем вид всех ссылок
	          var t  = this.text_svernut;
	          var t2 = this.text_svernut_all;
	          var t3 = this.text_svernut_vetku;
	       } else {
	          var display = "none";
	          //Меняем вид всех ссылок
	          var t  = this.text_razvernut;
	          var t2 = this.text_razvernut_all;
	          var t3 = this.text_razvernut_vetku;
	       }
	       //Все, кроме корневого
	      this.commentArea.find("ul ul .commentbody").css("display", display);
	       //Ссылка "Свернуть"/"Развернуть" у подчинённых комментариев
	      this.commentArea.find("ul ul .minimize").html(t);
	       //Ссылка "Свернуть ветку"/"Развернуть ветку" у родительских комментариев
	      this.commentArea.find("ul > li > div > .minimize").html(t3);
	       //Ссылка "Развернуть всё"       
	      this.commentArea.find(".commentsTurnOnOff").html(t2);
      }
    },
    
    //Вставка комментария из ajax
    insertCommentAfterNecessary: function(text, idParent, idChild, level) {
      //Если ul с таким rel нет, создаём его
      var elem = this.commentArea.find(".commentFormBody + ul[rel='" + level + "']");
      if (elem.length == 0) {
        $(".commenthtml_" + idParent).after(document.createElement("ul"));
        elem = $(".commenthtml_" + idParent).parent().find("ul:last");
        elem.attr("rel", level);
      }
      elem.append(document.createElement("li"));
      //В этот li вставляем наш текст
      elem.find("li:last").append(text);

      //(Циферка в числе комментариев + 1)
      updateNumberComments(this.commentAreaClass);

      //Скрываем форму, показываем комментарий, перескакиваем на место комментария
      this.commentArea.find(".commentFormBody").hide();
    },
    
    //"Ответить"
    //Помещаем форму после нужного ответа, меняем поле id_parent, показать ссылку в конце формы
    moveCommentFormToComment: function(id) {
      //Вычислить уровень добавляемого комментария, поместить его в скрытое поле
      //alert(this.form.find(".comment_level_comment").length);
      var c = parseFloat($(".commenthtml_" + id).parent().parent().attr('rel')) + 1;
      
      this.form.find(".comment_level_comment").val(c);

      this.form.parent().insertAfter(".commenthtml_" + id);
      //Строчка на случай, если форма скрыта
      this.form.parent().show();
      //
      this.form.find(".comment_id_parent_comment").val(id);
      //Показываем текст родителя, если не раскрыт
      $(".commenthtml_" + id + ' .commentbody').show();
      $(".commenthtml_" + id + ' .minimize').html(this.text_svernut);
      //Показываем кнопку комментариев в конце
      this.commentArea.find(".moveCommentFormToEndLink").show();
    },
    
    //Помещаем форму в конец комментариев, зануляем id_parent, скрываем ссылку
    moveCommentFormToEnd: function() {
      var form = this.commentArea.find(".commentFormBody");
      var cCommentTag = $(".cCommentTag");
      if (cCommentTag.length) {
    	cCommentTag.after(form);
      } else this.commentArea.prepend(form);
      
      //Строчка на случай, если форма скрыта
      this.commentArea.find(".commentFormBody").show();
      this.form.find(".comment_id_parent_comment").val("");
      this.commentArea.find(".moveCommentFormToEndLink").hide();
    }
  }
  
  function commentTagLink(b) {
      if (b){
        $('.cCommentTag').html('<a href="#" onclick="$(\'.cCommentArea\').slideToggle(); return false">'+ $('.cCommentTag').html() +'</a>' );
    } else {
        $('.cCommentArea').show();
    }
  }
  
  function updateNumberComments(commentAreaClass, count) {
    //(Циферка в числе комментариев + 1)
    //<h2 class="cCommentTag">Комментарии (3)</h2>
    var commentFormId = $("." + commentAreaClass).find(".cCommentTag");
    if (commentFormId.length > 0) {
	  var arr = /([^\d]+)(\d+)([^\d]+)/.exec(commentFormId.html());
	  if (arr.length == 4) {
	    if ((typeof(count) == "undefined")) {
	      count = parseInt(arr[2]) + 1;
	    }
        commentFormId.html(arr[1] + count + arr[3]);
	  }
    }
  }