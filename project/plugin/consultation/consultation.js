function formaNewStart(param){
  $('.cSendForm .toggleDiv').hide();
  rel = $('.cSendForm h2').attr('rel');
  $('.cSendForm h2').attr('rel',  $('.cSendForm h2').text() );
  $('.cSendForm h2').text(rel).wrapInner('<a href="#"></a>');
  $('.cSendForm h2 a').click(function(){
    rel = $('.cSendForm h2').attr('rel');
    txt = $('.cSendForm h2 a').text();
    if ($('.cSendForm .toggleDiv').css('display') == 'none'){
      $('.cSendForm .toggleDiv').slideDown();
    } else {
      $('.cSendForm .toggleDiv').slideUp();
    }
    $('.cSendForm h2').attr('rel',txt);
    $('.cSendForm h2 a').text(rel);
    return false;
  });
  if (param) {
    $('.cSendForm h2 a').click();
  }
}

function checkConsultationForm() {
  if ($('#cConsultationName').val() == '' || $('#cConsultationName').val() == $('#cConsultationName').attr('rel') ) {
    alert('Ошибка: поле "Имя" не заполнено');
    return false;
  }
  if ($('#cConsultationAsk').val() == '' || $('#cConsultationAsk').val() == $('#cConsultationAsk').attr('rel') ) {
    alert('Ошибка: поле "Вопрос" не заполнено');
    return false;
  } 
  $('#cConsultationForm').submit();
}