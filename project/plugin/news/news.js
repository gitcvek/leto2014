function cNewsCategoryBind(idCategory){
  $('.cNewsCategory').buttonset()
    .find('a[rel="'+idCategory+'"]').addClass('ui-button-disabled ui-state-disabled').click(function(){return false});
}

function mNewsCategoryBind(){
  $('.mNewsCategory').change(function(){
    window.location=$(this).val();
  });
}