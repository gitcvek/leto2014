/* Выполняется после загрузки страницы */
function adminDrawInit(){
  //instanceTableColorize(); //Делаем зебру на таблицах админки
  instanceSubDataBind();  //Добавление событий для рисования подчинённых сущностей
  aMenuInit(); //Схлопывает меню в админке
  btnHighlightBind();//События для подсветки кнопок
  notNullBind();//Событие обработки обязательных полей
  checkBoxToggleBind();//Обработка событий для переключающих видимость чекбоксов
  aPageSwBind();//Обработка событий для переключателя страниц
  aDatePickerBind();//Обработка событий для поля с датой и временем
  //aCacheOptionsBind();//Обработка опций кэширования
  aScriptTypeChangeBind();//Обработка событий смены типа php-скрипта (интерфейса)
  daHintBind(); // Заменяем все стандартные хинты браузера на красивые
}

function updateSequence(idObject, notify) {
  var m_seq = {};
  var trs = $('.aInstanceList tr[id^="ngin_inst_"]');
  for (var i=0; i < trs.length; i++) {
    m_seq[$(trs[i]).attr("id")] = i;
  }
  da_sortInstances(idObject, m_seq, notify);
  aShowHint("Порядок сортировки изменён", "highlight");
}

/* События для упорядочивания разделов*/
function instanceSequence(idObject, isAjax) {
  $(".aInstanceList tbody")
    .sortable({
      cursor: 'move',
      opacity: 0.4,
      placeholder: 'ui-state-highlight',
      forcePlaceholderSize: true,
      tolerance: 'pointer',
      axis: 'y',
      handle: '.ui-state-default',
      update: function(event, ui) {
        if (isAjax) updateSequence(idObject, false);
      },
      start: function(event, ui) {
        var firstTrTd = $('.ui-state-highlight').parent().find('tr:first:not(".ui-state-highlight") > td');
        var tdNum = firstTrTd.length;
        var tdHeight = firstTrTd.height();
        var add = "";
        for (var i=0; i < tdNum; i++) add += '<td style="height:'+tdHeight+'px"></td>';
        $('.ui-state-highlight').append(add);
      }
    });
}

/* Рисование зебры */
function instanceTableColorize(){
  $('.aInstanceList > th').addClass('alt_'+$('body').attr('lang'));
}

/* Рисование всплывающих меню для подчинённых сущностей */
var tim;
function instanceSubDataBind(){
  $('.aInstanceListRef.aSubData i').css('cursor','pointer')
    .mouseover(function(){
      $('.aSubData ul').hide();
      window.clearInterval(tim);
    rel = $(this).attr('rel');
      $('#'+rel).slideDown('fast',function(){
        $('.aSubData ul:not("[id=\''+rel+'\']")').hide();
      });
  })
  .mouseout(function(){
      aMenuTimer('#'+$(this).attr('rel'));
    });
  $('.aInstanceListRef.aSubData ul')
    .mouseover(function(){window.clearInterval(tim)})
  .mouseout(function(){aMenuTimer('#'+$(this).attr('id'))});
}
function aMenuTimer(el){
  window.clearInterval(tim);
  tim = setInterval(function(){
    $(el).fadeOut('slow');
    window.clearInterval(tim);
  }, 500);
}

/* 
  Перезагрузка текущей страницы
  currentPageLink - глобальная переменная, содеражащая текущий url 
*/
function reloadPage() {
  var url_bit = currentPageLink + "?i=" + Math.floor(Math.random()*100000);
  window.location = url_bit.replace( new RegExp( "&amp;", "g" ) , "&" );
}
/* Подтверждение удаления */
function daConf(dLink, deleteConfirmation) {
  if (confirm(deleteConfirmation)) location.href = dLink;
}

/* Упорядочивание экземпляров */
function drawModuleSeq(count, index, nameMod) {
  document.write('<select name="' + nameMod + '">');
  for (i = 0; i < count; i++) {
    add = "";
    if (i == index) add = ' selected="selected"';
      document.write('<option value="'+(i+1)+'"'+add+">"+(i+1)+"</option>");
  }
  document.write("</select>");
}

/* Удаление переведённых данных в данной локализации
localizationObject определяется в instance_admin_draw.php
*/
function admDeleteLocalizationData(id) {
  loadingData(true);
  da_deleteLocalizationData(mainIdObject, id);
  return false;
}

/* Отправка поискового запроса */
function sendSP() {
  $('#psv').attr('name', $('#psn').val());
  document.psform.submit();
}

/* Процесс ожидания ajax запроса с полным блокированием экрана
loadingText определяется в instance_admin_draw.php
*/
function loadingData(state) {
  wnd = $(window);
  if (state) {
    $('body > *').fadeOut('slow');
    $('body').append('<div id="loadingDiv"><div class="loadingPic"></div><div class="loadingText">' + loadingText + '...</div></div>');
    $('#loadingDiv').fadeIn('slow');
    wnd.resize(function() {
      $('#loadingDiv').css('top', (wnd.height()/2 - $('#loadingDiv').height()/2));
      $('#loadingDiv').css('left',(wnd.width()/2 - $('#loadingDiv').width()/2));
    });
    wnd.resize();
  } else {
//    $('#loadingDiv').remove();
//    $('body > *').fadeIn("slow");
    wnd.unbind("resize");
  }
}

/* Схлопывает все менюшки в админке, кроме тех, у которых класс ul.cur */
function aMenuInit(){
  // Отмечаем текущую категорию меню
  $('.aMenu li b').parents('.aMenuCategory').addClass('cur').prev().addClass('cur');

  // Блокируем меню, которое пользователь ранее сознательно открыл
  $('.aMenu .hdr').each(function(){
    var idMenuCategory = $(this).attr('rel');
    var openedCategory = $.cookie('daMenuCategoty['+idMenuCategory+']');
    if (openedCategory == 1) $(this).next().addClass('cur');
  });
  

  // Схлопываем всё лишнее
  var i = setInterval(function(){ $('.aMenu ul:not(.cur)').slideUp(); clearInterval(i) }, 100 );
  $('.aMenu .hdr').click(function(){
    curShlop = ($(this).next().css('display') != 'none');
    var idMenuCategory = $(this).attr('rel');
    $.cookie('daMenuCategoty['+idMenuCategory+']', (curShlop?null:1), {expires:1000, path:'/'});
    if (!curShlop) $(this).next().slideDown();
    else $(this).next().slideUp();
  }).css('cursor','pointer');
}

popInt = 0;
/* Подсветка кнопки и обработка события нажатия */
function btnHighlightBind(){
  $('.btn').each(function(){
    tegA = $(this).find('a');
    onclk = tegA.attr('onclick');
    if ((onclk == null) || (onclk == '')) $(this).find('td').click(function(){window.location.href = $(this).parent().find('a').attr('href')});
    else {
      $(this).find('td').unbind('click');
      $(this).find('td').click(onclk);
      tegA.parent().prepend(tegA.html());
      tegA.hide().html('');
    }
  });
  $('.btn td')
    .hover(function(){
      tbl = $(this).parent().parent().parent();
      tbl.find('.btnLeft').css('background-position','left -196px');
      tbl.find('.btnCenter').css('background-position','left -221px');
      tbl.find('td.btnRight').css('background-position','right -246px');
    }, function(){
      tbl = $(this).parent().parent().parent();
      tbl.find('.btnLeft').css('background-position','left -96px');
      tbl.find('.btnCenter').css('background-position','left -121px');
      tbl.find('td.btnRight').css('background-position','right -146px');
    });
//    .click(function(){
      /* Если определено событи onclick, то выполняется оно, иначе переход по ссылке */
/*    onclk = $(this).parent().find('a').attr('onclick');
      if ((onclk == null) || (onclk == '')) window.location.href = $(this).parent().find('a').attr('href');
    else {
    $(this).click(onclk);
    }
    });  */
  $('.btn th')
    .hover(function(){
      $(this).css('background-position','left -271px');
    }, function(){
      //if( $('#'+$(this).attr('rel')+':visible').length == 0 )
    $(this).css('background-position','left -171px');
    pm = $('#'+$(this).attr('rel'));
    clearInterval(popInt);
    popInt = setInterval(function(){pm.fadeOut('slow'); clearInterval(popInt)},500);
    })
    .click(function(){
      tbl = $(this).parent().parent().parent();
      $('#'+$(this).attr('rel')).css('left',tbl.offset().left).css('top',tbl.offset().top+tbl.height()).slideDown();
    });
  $('.aPopMenu')
    .mouseout(function(){
      pm = $(this);
    clearInterval(popInt);
      popInt = setInterval(function(){pm.fadeOut('slow'); clearInterval(popInt)},500);
    })
    .mouseover(function(){
      clearInterval(popInt);
    });
}

//Проверка и подсветка обязательных полей
function notNullBind(){
  function colorChange(el){
    if (el.val() == "") el.removeClass('green').addClass('red');
    else el.removeClass('red').addClass('green').parent().find('.msgNotNull').hide();
  }
  $('.notNullField').after('<b class="msgNotNull"></b>')
    .keyup(function(){colorChange($(this));})
    .blur(function(){colorChange($(this));})
    .click(function(){colorChange($(this));})
    .change(function(){colorChange($(this));})
    .change();
}

//Проверка обязательных полей перед сохранением
function checkBeforSave(msgText, alertText){
  //check tinymce
  $('.iTextarea.notNullField').each(function(){
  var ifr = $('#'+$(this).attr('id')+'_ifr');
    if ( ifr.length > 0 ){
      var ifrCont = document.getElementById( ifr.attr('id') ).contentWindow.document.body.innerHTML;
    var txt = '';
    $('<div>'+ifrCont+'</div>').find('*').each(function(){
        txt += jQuery.trim($(this).text());
    if (txt) return false;
      });
      if (txt) $(this).removeClass('red');
    else $(this).addClass('red');
  }
  });
  //check edit
  nnf = $('.notNullField.red');
  if (nnf.length > 0){
    nnf.parent().find('.msgNotNull').html(msgText).show();
    alert(alertText);
  return false; 
  }
  return true;
}

//Обработка событий для переключающих видимость чекбоксов
// ! При переходе по чекбоксу табом, работает неправильно
function checkBoxToggleBind() {
  function cbToggle(el){
    if (!el.attr('checked')) $('#'+el.attr('rel')).find('*').attr('disabled','');
    else $('#'+el.attr('rel')).find('*').attr('disabled','disabled');
  }
  $('.cbToggle').each(function(){
    $(this)
      .mouseup(function(){cbToggle($(this))})
      .keyup(function(){cbToggle($(this))});
    if (!$(this).attr('checked')) $('#'+$(this).attr('rel')).find('*').attr('disabled','disabled');
    else $('#'+$(this).attr('rel')).find('*').attr('disabled','');
  });
}

//Механизм расстановки модулей в шаблоне
function moduleSequenceStart() {
    function heightUl() {
/*      var h = 0;
      $(".modSeq .aModulePlace ul").height("");
      $(".modSeq .aModulePlace ul").each(function () {
          if ($(this).height() > h) h = $(this).height();
      })
      $(".modSeq .aModulePlace").find("ul").height(h + 25);*/

    }
    $(".modSeq .aModulePlace ul").sortable({
        connectWith: 'ul',
        stop: function (event, ui) {
            heightUl();
            $(".modSeq .aModulePlace").each(function () {
                m = 1;
                //did = $(this).attr('id');
                did = $(this).attr('id').substr(6, $(this).attr('id').length - 6);

                $(this).find('li').each(function () {
                    $(this).find('sup').text(m);
                    $(this).find('input.contSeq').val(m);
                    $(this).find('input.contDid').val(did);
                    m++;
                })
            })
        }
    });
    heightUl();
}

//Обработка событий загрузки файлов
function aFileUploadBind(){
  $('.aFileUpload .fileList li').each(function(){
    var int;
    var b;
    
    $(this).unbind('mouseover').unbind('mouseout')
      .mouseover(function(){ $(this).find('b').fadeIn('slow'); })
      .mouseout(function(){
        clearInterval(int);
        b = $(this).find('b');
        int = setInterval(function(){ b.fadeOut(); clearInterval(int); }, 500);
      })
      .find('a').unbind('mouseover').unbind('mouseout')
        .mouseover(function(){ clearInterval(int); })
        .mouseout(function(){
          clearInterval(int);
          b = $(this).parent().find('b');
          int = setInterval(function(){ b.fadeOut(); clearInterval(int); }, 500);
        }).end()
      .find('b').unbind('mouseover').unbind('mouseout')
        .mouseover(function(){ clearInterval(int); })
        .mouseout(function(){
          clearInterval(int);
          b = $(this);
          int = setInterval(function(){ b.fadeOut(); clearInterval(int); }, 500);
        });
  });
}

//Обработка событий для переключателя страниц
function aPageSwBind(){
  $('.aPageSw .pageRec').change(function(){
    alert('/engine/js/ngin.js:310 строка; Тут должно на аяксе смениться количество отображаемых записаей');
  });
}



//Обработка событий для поля с датой и временем
function aDatePickerBind(){
  if ( $('.admDate').length ){
    $('.admDate')
      .datepicker({
        dateFormat: 'dd.mm.yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true
      })
      .change(function(){
        var name = $(this).attr('rel');
        var day = $(this).val().slice(0,2);
        var month = $(this).val().slice(3,5);
        var year = $(this).val().slice(6,11);
        $('[name="'+name+'_dayS"]').val(day);
        $('[name="'+name+'_monthS"]').val(month);
        $('[name="'+name+'_yearS"]').val(year);
      });
  }
}

function aScriptTypeChangeBind(){
  $('[name="p337"]')
    .change(function(){
      if ( ($(this).val() == 1) || ($(this).val() == 2) ){
        $('.admCacheOptionsTr, .admCacheOptionsTr .val').show();
      } else {
        $('.admCacheOptionsTr .val').slideUp('slow', function(){$('.admCacheOptionsTr').hide()});
      }
    })
    .change();
}

function aCacheOptionsBind(){
  $('.aCacheMode input').click(function(){
    $(this).parents('.admCacheOptions').find('.aCacheModeSettings').slideUp();
    var rel = $(this).attr('rel');
    $('#'+rel).slideDown();
  });
  $('.aCacheInDB').click(function(){
    var dbSettings = $(this).attr('rel');
    if ($(this).is(':checked')){
      $('#'+dbSettings).slideDown();
    } else {
      $('#'+dbSettings).slideUp();
    }
  });
  $('.aCacheMode').buttonset();
}

/*
 * Показать сообщение пользователю
 * txt - текст сообщения
 * msgType - [highlight - просто сообщение; error - сообщение об ошибке]
 * time - время показа в секундах (по умолчанию 3)
 */
function aShowHint(txt, msgType, time){
  var icon = "info";
  if (msgType == 'error') icon = 'alert';
  var widget = "";
  //position:absolute; top:'+$(window).scrollTop()+'px; right:10px; 
  widget += '<div class="ui-widget aHint">';
  widget += '  <div class="ui-state-'+msgType+' ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> ';
  widget += '    <p><span class="ui-icon ui-icon-'+icon+'" style="float: left; margin-right: .3em;"></span>';
  widget += '    '+txt+'</p>';
  widget += '  </div>';
  widget += '</div>';
  
  $('body').append(widget).find('.aHint').fadeIn('slow', function(){
    if (!time)  time = 3;
    var int = setInterval(function(){ 
      $('.aHint').fadeOut('slow', function(){ $(this).remove() }); 
      clearInterval(int);
    }, time*1000 );
  });
}

/*
 * Заменяем все стандартные хинты браузера на красивые
 */
function daHintBind(){
  var popravkaX = 20; //смещение от курсора по ширине
  var popravkaY = 20; //смещение от курсора по высоте
  var popravkaW = 50; //запас бордюров для расчёта ширины
  var titleHint = null;

  function hintPosition(titleHint, kmouse){
    var tipTop = 0;
    var tipLeft = 0;
    var wnd = $(window);

    if ( titleHint.width() > (wnd.width()/2-popravkaW) ) titleHint.width( wnd.width()/2-popravkaW );

    if ((kmouse.pageX + titleHint.width()+popravkaX) > ($(window).width() + $(window).scrollLeft())) tipLeft = kmouse.pageX - titleHint.width() - popravkaX;
    else tipLeft = kmouse.pageX+popravkaX;

    if ((kmouse.pageY + titleHint.height()+popravkaY) > ($(window).height() + $(window).scrollTop())) tipTop = kmouse.pageY - titleHint.height() - popravkaY;
    else tipTop = kmouse.pageY+popravkaY;

    titleHint.css({left:tipLeft, top:tipTop});
  }

  $('[title]').each(function(){
    var title = $(this).attr('title');
    if (title != ""){
      $(this)
        .removeAttr('title')
        .mouseover(function(kmouse){
          $("body").append('<div class="aTitleHint ui-widget"><div class="ui-state-highlight ui-corner-all">'+title+'</div></div>');
          titleHint = $('.aTitleHint');
          hintPosition(titleHint, kmouse);
          titleHint.css('opacity', 0.9).show();
        })
        .mousemove(function(kmouse){
          hintPosition(titleHint, kmouse);
        })
        .mouseout(function(){
          titleHint.remove();
        });
    }
  });
}

function cleanCode(tmpC, d1, d2, d3, d4, d6, d6, d7, d8, d9) {
  tmpC = $('<div>' + tmpC + '</div>');//get jquery object

  //delete width, height
/*  if(typeof(d1) != "undefined" && d1){
    tmpC.find('*[width]').not('img,object').removeAttr("width");
    tmpC.find('*[height]').not('img,object').removeAttr("height");
    tmpC.find('*').not('img,object').css({width:'',  height:''});
  }
*/
  //delete css paddings, margins
  if(typeof(d2) != "undefined" && d2){
    tmpC.find('*').not('img,object').css({margin:'', padding:''});
  }
  
  if(typeof(d3) != "undefined" && d3){
    tmpC.find('*').css({background:'', textIndent:'', fontSize:'', lineHeight:'', fontFamily:'', color:''});
  }
  
  //delete background
  if(typeof(d4) != "undefined" && d4){
    tmpC.find('*[bgcolor]').removeAttr("bgcolor");
    tmpC.find('*').css('background','');
  }
  
  //delete border
  if(typeof(d5) != "undefined" && d5){
    tmpC.find('*[border]').removeAttr("border");
    tmpC.find('*').css('border','');
  }
  
  //delete lang, class Mso
  if(typeof(d6) != "undefined" && d6){
    tmpC.find('*[lang]').removeAttr("lang");
    tmpC.find("*[class*='Mso']").removeAttr("class");
  }
  
  //delete underline
/*  if(typeof(d7) != "undefined" && d7){
    tmpC.find('u').each(function () {
      $(this).replaceWith($(this).html());  
    });
    tmpC.find('*').each(function(){
      if ($(this).css('text-decoration') == 'underline') $(this).css('text-decoration','');  
    });
  }
*/
  //delete &nbsp;
  if(typeof(d8) != "undefined" && d8){
    tmpC.html( tmpC.html().replace(/&nbsp;/g,' ') );
  }
  
  //delete comments
  if(typeof(d9) != "undefined" && d9){
    tmpC.html( tmpC.html().replace(/<!--.*-->/g, "") );        
  }
  
  return tmpC.html();
}
