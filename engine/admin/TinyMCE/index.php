<?php 
  include_once dirname(__FILE__) .'/inc.php';  // Объявляет переменные $plugins и $flagsValue (последняя пока не используется)
  
  // Полный состав рядов
  $row1 = array('justifyleft', 'justifyright', 'justifycenter', 'justifyfull', '|', 'bold', 'italic', 'underline', 'strikethrough', '|', 'fontsizeselect', 'fontselect', '|', 'formatselect', '|', 'forecolor', 'backcolor');
  
  $addStyles = getSysParamValue("PR_TINY_STYLES");
  if ($addStyles != null && trim($addStyles) != "") {
    $row1[] = "|";
    $row1[] = "styleselect";
  }
  
  $row2 = array('print', 'preview', '|', 'save', 'newdocument', '|', 'cut', 'copy', 'paste', 'pastetext', 'pasteword', 'selectall', '|', 'undo', 'redo', '|', 'outdent', 'indent', '|', 'hr', 'link', 'unlink', 'anchor', '|', 'image', 'media', '|', 'sub', 'sup', 'bullist', 'numlist');
  $row3 = array('tablecontrols', '|', 'removeformat', 'nGin_clean', 'cleanup', 'charmap', 'code');
  
  $disableButtonsFromSystem = array('fontsizeselect', 'fontselect', 'save', 'newdocument', 'cleanup');  // Запрещенные по умолчанию кнопки
  if (defined("DA_TINYMCE_BUTTON_DISABLE")) {
    $disableButtonsFromSystem = explode(",", DA_TINYMCE_BUTTON_DISABLE);
  }
  function _tiny_rowProcess(array $row, array $disableButtonsFromSystem, array $availableButtonsFromUser) {
    $result = "";
    $c = count($row);
    for ($i = 0; $i < $c; $i++) {
      $button = $row[$i];
      if (in_array($button, $disableButtonsFromSystem) && !in_array($button, $availableButtonsFromUser)) {
      } else {
        if ($result != "") $result .= ",";
        $result .= $button;
      }
    }
    return $result;
  }
  $availableButtonsFromUser = array();
  if (defined("DA_TINYMCE_BUTTON_AVAILABLE")) {
    $availableButtonsFromUser = explode(",", DA_TINYMCE_BUTTON_AVAILABLE);
    // Пример
//  define('DA_TINYMCE_BUTTON_AVAILABLE', "fontsizeselect,fontselect");  // Разрешить возможности, отключенные по умолчанию. Кнопки перечисляются через запятую, обязательно без пробелов    
  }
  $buttons_row1 = _tiny_rowProcess($row1, $disableButtonsFromSystem, $availableButtonsFromUser);
  $buttons_row2 = _tiny_rowProcess($row2, $disableButtonsFromSystem, $availableButtonsFromUser);
  $buttons_row3 = _tiny_rowProcess($row3, $disableButtonsFromSystem, $availableButtonsFromUser);

  $projectTinyPlugins = array();
  if (defined("DA_TINYMCE_PROJECT_PLUGINS")) {
    $tmp = explode(";", DA_TINYMCE_PROJECT_PLUGINS);
    $c = count($tmp);
    $a = "";
    $b = "";
    for ($i = 0; $i < $c; $i++) {
      $tmpInst = trim($tmp[$i]);
      $tmpPos = mb_strpos($tmpInst, "=");
      $paramName = mb_substr($tmpInst, 0, $tmpPos);
      $paramValue = mb_substr($tmpInst, $tmpPos+1);
      if ($paramName == "button") {
        $buttons_row3 .= ",".$paramValue;
      } else if ($paramName == "plugin") {
        $plugins[] = "-".$paramValue;
        $a = $paramValue;
      } else if ($paramName == "path") {
        $b = $paramValue;
      }
      if ($a != "" && $b != "") {
        $projectTinyPlugins[$a] = $b;
        $a = "";
        $b = "";
      }
    }
    
  }
  
/*  
  
  $buttons_row1 = 
  'justifyleft,justifyright,justifycenter,justifyfull,|,'.  // Выравнивание
  'bold,italic,underline,strikethrough,|,'.                 // Жирность, курсив...
//  'fontsizeselect,fontselect,'.                           // Размер шрифта
//  'styleselect,|,'.                                       // Стиль текста, абзац, шрифт
  'formatselect,|,'.
  'forecolor,backcolor'.$addTiny;                           // Цвет текста и фона, доп. стили
  
  $buttons_row2 = 
  'print,preview,|,'.                         // Печать, предв. просмотр.
  //'save,newdocument,print,preview,|,'.
  'cut,copy,paste,pastetext,pasteword,selectall,|,' .   // Вырезать, вставить текст...
  'undo,redo,|,outdent,indent,|,' .           // Повторить, отменить действие. Добавить, удалить отступ.
  'search,|,hr,link,unlink,|,image,media,|,sub,sup,bullist,numlist,visualchars';  // Найти (заменить - внутри), линия, вставить ссылку, картинку, нижний/верхний индекс, марикрованный/нумерованный список.
*/
//  $buttons_row3 = 
//  'tablecontrols,|,removeformat,nGin_clean,/*cleanup,*/charmap,code';


/*  // Далее корректировка - временно отключаем.
  // Ряд 1
  if (!isFlagEnabled($flagsValue, DA_TEGS_ALIGN)){
    $buttons_row1 = str_replace('justifyleft,justifyright,justifycenter,justifyfull,|,', '', $buttons_row1);
  }  
  if (!isFlagEnabled($flagsValue, DA_TEGS_BASE_TEXT)){
    $buttons_row1 = str_replace('bold,italic,underline,strikethrough,|,', '', $buttons_row1);
  }
  if (!isFlagEnabled($flagsValue, DA_TEGS_COLOR)){
    $buttons_row1 = str_replace(',forecolor,backcolor', '', $buttons_row1);
  }  
  
  // Ряд 2
  if (!isFlagEnabled($flagsValue, DA_TEGS_LINK)){
    $buttons_row2 = str_replace(',link,unlink,|', '', $buttons_row2);
    unset($plugins[array_search('advlink', $plugins)]);
  }
  if (!isFlagEnabled($flagsValue, DA_TEGS_IMAGE)){
    $buttons_row2 = str_replace(',image,|', '', $buttons_row2);
    unset($plugins[array_search('advimage', $plugins)]);
    unset($plugins[array_search('media', $plugins)]);    
  }
  
  // Ряд 3
  if (!isFlagEnabled($flagsValue, DA_TEGS_TABLE)){
    $buttons_row3 = str_replace('tablecontrols,|,', '', $buttons_row3);
    unset($plugins[array_search('table', $plugins)]);
  }

  if (!isFlagEnabled($flagsValue, DA_TEGS_BLOCK)){
    // ?
  }
  if (!isFlagEnabled($flagsValue, DA_TEGS_HTML)){
    $buttons_row2 = str_replace(',sub,sup,bullist,numlist', '', $buttons_row2);    
  }  
  if (!isFlagEnabled($flagsValue, DA_TEGS_SCRIPT)){    
  }*/
  
  // Язык в формате ISO-639-1
  global $locale;
  $tmpCurLocale = $locale->getLocale();
  $locale->setRepositoryLocale();
  $code = $locale->getCode();
  $locale->setLocale($tmpCurLocale);

  $addTiny = "";
  if ($addStyles != null && trim($addStyles) != "") {
    $addTiny = "  theme_advanced_styles : \"".$addStyles."\",";
  }

  $addTinyPlugin = "";
  $tmp = getSysParamValue("PR_TINY_STYLES_LINK");
  if ($tmp != null && trim($tmp) != "") {
    $addTinyPlugin = "  advlink_styles : \"".$tmp."\",\n";
  }
  
  defineif("DA_TINYMCE_COMPRESS", true);   // По умолчанию сжатие включено
  
  /*

Описание действий опция TinyMCE (http://wiki.moxiecode.com/index.php/TinyMCE:Configuration): core:
mode: exact  - в редактор превратятся только элементы, свойство name которых перечислено в опции elements
elements     - перечисляет имена элементов (textarea), которые будут представлены в виде визуального редактора
theme: advanced - тема настраивается пользователем. Темы лежат в папке ./themes/
plugins      - перечисляем список используемых плагинов. Плагины лежат в папке ./plugins/
relative_urls- false - все пути будут абсолютными. Если true - то отностильно TinyMce

rows: расположение кнопок по рядам (buttons1 - первый ряд и т.д.)

theme: расположение панели для темы "advanced"

other:

plugins:

*/

$tinyPasteRetainStyleProperties = "";
if (defined("DA_TINYMCE_PASTE_RETAIN_STYLE_PROPERTIES")) {
  $tinyPasteRetainStyleProperties = DA_TINYMCE_PASTE_RETAIN_STYLE_PROPERTIES;
} else {
  $tinyPasteRetainStyleProperties = "border, border-right, border-top, border-left, border-bottom";
}
// tinymce.PluginManager.load('nGin_clean', '/project/tiny/nGin_clean/editor_plugin.js');
if (DA_TINYMCE_COMPRESS) { // сжимаем редактор  
?>
<script type="text/javascript" src="/engine/admin/TinyMCE/tiny_mce_gzip.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE_GZ.init({
  plugins : '<?php echo implode(',', $plugins)?>',
  themes : 'advanced',
  languages : '<?php echo $code; ?>',
  disk_cache : true,
  debug : false
});
<?php } else { // не сжимаем редактор ?>
<script language="javascript" type="text/javascript" src="/engine/admin/TinyMCE/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<?php }
  foreach($projectTinyPlugins AS $key => $value) {
    echo 'tinymce.PluginManager.load("'.$key.'", "'.$value.'");'."\n";
  }
?>
tinyMCE.init( {
  // core
  mode : "exact", // "textareas"
  elements : "<?php echo implode(',', $blobTextareaIds)?>",
  theme : "advanced",
  plugins : "<?php echo implode(',', $plugins)?>",
<?php /* setup:function(ed){
    ed.onKeyUp.add(function(ed){checkEditor(ed)});
    ed.onNodeChange.add(function(ed){checkEditor(ed)});
    ed.onInit.add(function(ed){checkEditor(ed)});
  }, */ ?>
  relative_urls: false, 
  dialog_type : "modal",

  // rows
  theme_advanced_buttons1 : "<?php echo $buttons_row1?>",
  theme_advanced_buttons2 : "<?php echo $buttons_row2?>",
  theme_advanced_buttons3 : "<?php echo $buttons_row3?>",

  // add ngin parameter
<?php

  $addParam = TinyMceConfig::getAllParameter();
  foreach($addParam AS $name => $value) {
    echo "  ".$name." : ";
    if (is_bool($value)) {
      if ($value === true) echo "true";
        else echo "false";
    }
    echo ",\n";
  }

?>

  // theme
  theme_advanced_toolbar_location : "top",
  theme_advanced_toolbar_align : "left",
  theme_advanced_statusbar_location : "bottom",
  theme_advanced_resizing : true,
  theme_advanced_resize_horizontal : false,
<?php echo $addTiny; ?>

  // other
  accessibility_warnings : false,
  language: '<?php echo $code; ?>',
  content_css : "/css/content.css",
  debug : false,
  apply_source_formatting : true,

  // plugins
<?php echo $addTinyPlugin; ?>
  paste_auto_cleanup_on_paste : true,
  paste_convert_middot_lists : true,
  paste_retain_style_properties : "<?php echo $tinyPasteRetainStyleProperties; ?>",
  paste_strip_class_attributes : "all",
  paste_remove_spans : false,
  paste_remove_styles_if_webkit : false,
  paste_remove_styles : false,

<?php
  defineif("DA_TINYMCE_CLEAN_CODE_ON_PASTE", false);
  if (DA_TINYMCE_CLEAN_CODE_ON_PASTE) {
?>
  setup : function(ed) {
    ed.onPostProcess.add(function(ed, o) {
      o.content = cleanCode(o.content, true, true, true, true, true, true, true, true, true);
    });
  }
<?php
  }
?>
});

var TinyMceLinksArray = Array();  // кэширование ссылок
var TinyMceLinksNameArray = Array();

</script>
<?php
  // !Так было раньше (сейчас список файлов загружается от клиента, из глобальной js-переменной через метод DaFileList.getAllFiles()):
  // По document.getElementById(\"temp_value\") определяем единый временный ИД, который используется в частности при загрузке файлов
  // Файлы в редакторе загружаются по аяксу, а не берутся из списка файлов или ещё откуда-то
  // global $daPage;
  // $daPage->addPreloadJs("TinyMceTempValue = document.getElementById(\"temp_value\").value;");
?>
