(function() {
  tinymce.create('tinymce.plugins.nGin_cleanPlugin', {
    preInit : function() {
      var url;
      tinyMCEPopup.requireLangPack();
      document.write('<script language="javascript" type="text/javascript" src="' + tinyMCEPopup.editor.documentBaseURI.toAbsolute(url) + '"></script>');
    },
    init : function(ed, url) {
      var t = this;
      t.editor = ed; 
      // Register commands  
      ed.addCommand('nGin_cmd_clean', function() {
        ed.windowManager.open({file:url+"/i.htm",width:480,height:360,inline:1},{plugin_url:url});
      });

      // Register buttons
      ed.addButton('nGin_clean', {title : 'clean', cmd : 'nGin_cmd_clean', image : url + '/nGinClean.gif'});
    },      
    getInfo : function() {
      return {
        longname : 'nGin clean',
        author : 'Digital Age',
        authorurl : 'http://cvek.ru',
        infourl : 'http://cvek.ru',
        version : "1"
      };
    }
  });  
  

  tinymce.PluginManager.add('nGin_clean', tinymce.plugins.nGin_cleanPlugin);
})();