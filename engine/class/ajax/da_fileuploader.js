/**
 * Добавлена проверка аргумента на существование
 */
qq.getByClass = function(element, className){
  var result = [];
  
  if (typeof(element) != "undefined" && element !== null) {
    if (element.querySelectorAll){
      return element.querySelectorAll('.' + className);
    }
      
    var candidates = element.getElementsByTagName("*");
    var len = candidates.length;

    for (var i = 0; i < len; i++){
      if (qq.hasClass(candidates[i], className)){
        result.push(candidates[i]);
      }
    }
  }
  return result;
}

/**
 * Добавлена проверка аргумента на существование
 */
qq.attach = function(element, type, fn) {
  if (typeof(element) != "undefined" && element !== null) {
    if (element.addEventListener){
        element.addEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.attachEvent('on' + type, fn);
    }
  }
}

/**
 * Переписан главный конструктор, задающий все настройки. В оригинале 
 * 1) рисовалась анимированная кнопка
 * 2) файл начинал загружаться сразу после выбора в окне диалога
 * 
 * Здесь же:
 * 1) отображается стандартное поле ввода;
 * 2) пользователь сам задаёт, когда пойдёт загрузка (на нужном событии стоит эта функция)
 */
DaFileUploader = function(o) {
  o.element = document.getElementById(o.fileField);
  
  //Если элемент по id не найден, искать его по имени
  if (!(o.element)) {
    var elements = document.getElementsByTagName("input");
    for (i = 0; i < elements.length; i ++) {
      if (elements[i].name == o.fileField) {
        o.element = elements[i];
      }
    }
  }
  
  o.messages = {
      fileChoiceError: "Файл не выбран",
      typeError: "Файл {file} имеет неверное расширение. Доступные расширения: {extensions}.",
      sizeError: "Максимально допустимый размер файла {sizeLimit}.",
      minSizeError: "Минимально допустимый размер файла {minSizeLimit}.",
      emptyError: "Файл {file} пуст, выберите другой.",
      onLeave: "Если Вы покинете страницу, текущие загрузки файлов будут отменены."            
  }

  //Если  в пользовательских настройках не задано иное, множественную загрузку файлов отключить
  if (typeof(o.multiple) == "undefined") o.multiple = false;
  
  // call parent constructor
  qq.FileUploaderBasic.apply(this, arguments);

  //Поместить имя файла в параметры
  if (typeof(this._options.params) == "undefined") this._options.params = new Object();
  this._options.params.fileField = o.fileField;
  if (typeof(o.tempValue) != "undefined") this._options.params.tempValue = o.tempValue;
  //
  
  //Поместить в свойство элемент input:file
  this._element = this._options.element;
  
  //Если есть кнопка Cancel, можно навесить на ссылку отмену загрузки
  //Если данный функционал нужен, то необходимо переписать этот метод (643 строка оригинального файла)
  this._bindCancelEvent();
  
  //Перетаскивание пока не реализовано
  //this._setupDragDrop();
  
  if (this._element.value == '') {
    this._error('fileChoiceError', '');
    return false;
  }
  
  //Начать загрузку
  this._onInputChange(this._element);
}

/**
 * Организуем наследование (переписываем все методы нашему объекту)
 */
qq.extend(DaFileUploader.prototype, qq.FileUploader.prototype);

/**
 * Переписан для комментирования  последней строчки
 */
DaFileUploader.prototype._onInputChange = function(input) {
  if (this._handler instanceof qq.UploadHandlerXhr) {
    this._uploadFileList(input.files);
  } else {
    if (this._validateFile(input)){
      this._uploadFile(input);
    }
  }
  
  //this._button.reset();
}

/**
 * Куда добавлять данные о загружаемом на данный момент файле
 * @param id - в случае загрузки через xhr - порядковый номер загружаемого файла (начиная с 0)
 * в случае загрузки через iframe - id iframe
 * @param fileName - имя загружаемого файла
 */
DaFileUploader.prototype._addToList = function(id, fileName) { }

/**
 * Отображение процесса загрузки
 */
DaFileUploader.prototype._onProgress = function(id, fileName, loaded, total) { }

/**
 * В оригинале убирал крутящийся spinner и ссылку cancel
 * @param id - id загружаемого файла (порядковый номер для xhr, id фрейма- для iframe-загрузки)
 * @param fileName - имя загруженного файла
 * @param result - массив результатов от php-скрипта
 */
DaFileUploader.prototype._onComplete = function(id, fileName, result) {
  //Чтобы ошибки не просочились в родительскую функцию
  var draft = new Array(id, fileName, {});
  //Метод закрывает загружаемый файл, должен быть вызван обязательно
  qq.FileUploaderBasic.prototype._onComplete.apply(this, draft);
  
  //Черновик
  if (this._filesInProgress == 0) {
    if ('onCompleteAll' in this._options) {
      this._options.onCompleteAll();
    }
  }
}

/**
 * Класс для форм, отправляющих файл через iframe
 * Переопределяются методы самого класса без создания надстройки (много где используется, везде править название
 * не представляется возможным)
 * Данный метод в оригинале переименовывал поле ввода и убирал его со страницы
*/
qq.UploadHandlerForm.prototype.add = function(fileInput) {
  var id = 'qq-upload-handler-iframe' + qq.getUniqueId();       
  
  this._inputs[id] = fileInput;
  
  // remove file input from DOM
  //if (fileInput.parentNode){
      //qq.remove(fileInput);
  //}
  
  return id;
}

/** Метод переписан из-за одного аспекта: в оригинале поле перемещалось в форму, здесь же оно копируется
 * @param id - id iframe, куда пересылается файл
 * @param params - дополнительные данные, пересылаемые вместе с файлом
 */
qq.UploadHandlerForm.prototype._upload = function(id, params) {
  var input = this._inputs[id];

  if (!input){
      throw new Error('File with passed id was not added, or already uploaded or cancelled');
  }

  var fileName = this.getName(id);
  
  var iframe = this._createIframe(id);
  
  //Переслать имя файла
  params.qqfile = fileName;
  var form = this._createForm(iframe, params);
  
  //!!!
  var element = $(":file[name='" + input.name + "']");
  if (($.trim(input.id) != "") && (element.length == 0)) element = $("#" + input.id);

  //Само поле переместить в форму, на его место поставить копию
  element.replaceWith(element.clone(true));
  $("form[target='" + id + "']").append(element);
  //!!!
  
  var self = this;
  this._attachLoadEvent(iframe, function(){
      var response = self._getIframeContentJSON(iframe);
      //Перекодировать ответ
      for (key in response) if (response[key] != null) response[key] = htmlspecialchars_decode(response[key]);
    
      self._options.onComplete(id, fileName, response);
      self._dequeue(id);
      delete self._inputs[id];
      // timeout added to fix busy state in FF3.6
      setTimeout(function(){
          qq.remove(iframe);
      }, 1);
  });

  form.submit();        
  qq.remove(form);
  
  return id;
}

/**
 * Данный метод переопреляется исключительно ради применения htmlspecialchars_decode
 */
qq.UploadHandlerXhr.prototype._onComplete = function(id, xhr) {
  // the request was aborted/cancelled
  if (!this._files[id]) return;
  
  var name = this.getName(id);
  var size = this.getSize(id);
  
  this._options.onProgress(id, name, size, size);
          
  if (xhr.status == 200){
      var response;

      try {
          response = eval("(" + xhr.responseText + ")");
      } catch(err){
          response = {};
      }
      //Перекодировать ответ
      for (key in response) if (response[key] != null) response[key] = htmlspecialchars_decode(response[key]);
      
      this._options.onComplete(id, name, response);
  } else {                   
      this._options.onComplete(id, name, {});
  }
          
  this._files[id] = null;
  this._xhrs[id] = null;    
  this._dequeue(id);
}

/**
 * Ответ скрипта возвращается в виде спецсимволов (&lt; и т.д.), функция расшифровки
 */
function htmlspecialchars_decode(string, quote_style) {
  var histogram = {}, symbol = '', tmp_str = '', entity = '';
  tmp_str = string.toString();
  
  table = 'HTML_SPECIALCHARS';
  
  //function get_html_translation_table(table, quote_style) {
    var entities = {}, histogram = {}, decimal = 0, symbol = '';
    var constMappingTable = {}, constMappingQuoteStyle = {};
    var useTable = {}, useQuoteStyle = {};
    
    useTable      = (table ? table.toUpperCase() : 'HTML_SPECIALCHARS');
    useQuoteStyle = (quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT');
    
    // Translate arguments
    constMappingTable[0]      = 'HTML_SPECIALCHARS';
    constMappingTable[1]      = 'HTML_ENTITIES';
    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
    constMappingQuoteStyle[2] = 'ENT_COMPAT';
    constMappingQuoteStyle[3] = 'ENT_QUOTES';
    
    // Map numbers to strings for compatibilty with PHP constants
    if (!isNaN(useTable)) {
        useTable = constMappingTable[useTable];
    }
    if (!isNaN(useQuoteStyle)) {
        useQuoteStyle = constMappingQuoteStyle[useQuoteStyle];
    }
    
    if (useQuoteStyle != 'ENT_NOQUOTES') {
        entities['34'] = '&quot;';
    }

    if (useQuoteStyle == 'ENT_QUOTES') {
        entities['39'] = '&#039;';
    }

    if (useTable == 'HTML_SPECIALCHARS') {
        // ascii decimals for better compatibility
        entities['38'] = '&amp;';
        entities['60'] = '&lt;';
        entities['62'] = '&gt;';
    } else if (useTable == 'HTML_ENTITIES') {
        // ascii decimals for better compatibility
        entities['38']  = '&amp;';
        entities['60']  = '&lt;';
        entities['62']  = '&gt;';
        entities['160'] = '&nbsp;';
        entities['161'] = '&iexcl;';
        entities['162'] = '&cent;';
        entities['163'] = '&pound;';
        entities['164'] = '&curren;';
        entities['165'] = '&yen;';
        entities['166'] = '&brvbar;';
        entities['167'] = '&sect;';
        entities['168'] = '&uml;';
        entities['169'] = '&copy;';
        entities['170'] = '&ordf;';
        entities['171'] = '&laquo;';
        entities['172'] = '&not;';
        entities['173'] = '&shy;';
        entities['174'] = '&reg;';
        entities['175'] = '&macr;';
        entities['176'] = '&deg;';
        entities['177'] = '&plusmn;';
        entities['178'] = '&sup2;';
        entities['179'] = '&sup3;';
        entities['180'] = '&acute;';
        entities['181'] = '&micro;';
        entities['182'] = '&para;';
        entities['183'] = '&middot;';
        entities['184'] = '&cedil;';
        entities['185'] = '&sup1;';
        entities['186'] = '&ordm;';
        entities['187'] = '&raquo;';
        entities['188'] = '&frac14;';
        entities['189'] = '&frac12;';
        entities['190'] = '&frac34;';
        entities['191'] = '&iquest;';
        entities['192'] = '&Agrave;';
        entities['193'] = '&Aacute;';
        entities['194'] = '&Acirc;';
        entities['195'] = '&Atilde;';
        entities['196'] = '&Auml;';
        entities['197'] = '&Aring;';
        entities['198'] = '&AElig;';
        entities['199'] = '&Ccedil;';
        entities['200'] = '&Egrave;';
        entities['201'] = '&Eacute;';
        entities['202'] = '&Ecirc;';
        entities['203'] = '&Euml;';
        entities['204'] = '&Igrave;';
        entities['205'] = '&Iacute;';
        entities['206'] = '&Icirc;';
        entities['207'] = '&Iuml;';
        entities['208'] = '&ETH;';
        entities['209'] = '&Ntilde;';
        entities['210'] = '&Ograve;';
        entities['211'] = '&Oacute;';
        entities['212'] = '&Ocirc;';
        entities['213'] = '&Otilde;';
        entities['214'] = '&Ouml;';
        entities['215'] = '&times;';
        entities['216'] = '&Oslash;';
        entities['217'] = '&Ugrave;';
        entities['218'] = '&Uacute;';
        entities['219'] = '&Ucirc;';
        entities['220'] = '&Uuml;';
        entities['221'] = '&Yacute;';
        entities['222'] = '&THORN;';
        entities['223'] = '&szlig;';
        entities['224'] = '&agrave;';
        entities['225'] = '&aacute;';
        entities['226'] = '&acirc;';
        entities['227'] = '&atilde;';
        entities['228'] = '&auml;';
        entities['229'] = '&aring;';
        entities['230'] = '&aelig;';
        entities['231'] = '&ccedil;';
        entities['232'] = '&egrave;';
        entities['233'] = '&eacute;';
        entities['234'] = '&ecirc;';
        entities['235'] = '&euml;';
        entities['236'] = '&igrave;';
        entities['237'] = '&iacute;';
        entities['238'] = '&icirc;';
        entities['239'] = '&iuml;';
        entities['240'] = '&eth;';
        entities['241'] = '&ntilde;';
        entities['242'] = '&ograve;';
        entities['243'] = '&oacute;';
        entities['244'] = '&ocirc;';
        entities['245'] = '&otilde;';
        entities['246'] = '&ouml;';
        entities['247'] = '&divide;';
        entities['248'] = '&oslash;';
        entities['249'] = '&ugrave;';
        entities['250'] = '&uacute;';
        entities['251'] = '&ucirc;';
        entities['252'] = '&uuml;';
        entities['253'] = '&yacute;';
        entities['254'] = '&thorn;';
        entities['255'] = '&yuml;';
    } else {
        throw Error("Table: "+useTable+' not supported');
        return false;
    }
    
    // ascii decimals to real symbols
    for (decimal in entities) {
        symbol = String.fromCharCode(decimal)
        histogram[symbol] = entities[decimal];
    }
    
    //return histogram;
  //}
  
  if (histogram === false) {
      return false;
  }

  // &amp; must be the last character when decoding!
  delete(histogram['&']);
  histogram['&'] = '&amp;';

  for (symbol in histogram) {
      entity = histogram[symbol];
      tmp_str = tmp_str.split(entity).join(symbol);
  }
  
  return tmp_str;  
}