$(document).ready(function(){
  $("input[placeholder], textarea[placeholder]")
    .focus(function(){
       if ($(this).val() == $(this).attr('placeholder')) $(this).val("").removeClass('void');
     })
    .blur(function(){
       if ($(this).val() == '') $(this).val( $(this).attr('placeholder') ).addClass('void');
     }).blur();
});
